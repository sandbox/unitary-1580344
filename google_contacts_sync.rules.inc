<?php
/**
 * @file
 * rule for module.
 */

/**
 * Implements hook_rules_action_info().
 */
function google_contacts_sync_rules_action_info() {
  $common = array(
    'group' => t('Google Contacts'),
  );
  $info = array();
  $info['google_contacts_sync_drupal_to_google'] = array(
    'label' => t('Update Google Contacts'),
    'base' => 'google_contacts_sync_drupal_to_google',
  ) + $common;
  $info['google_contacts_sync_google_to_drupal'] = array(
    'label' => t('Update Drupal users'),
    'base' => 'google_contacts_sync_google_to_drupal',
  ) + $common;
  return $info;
}

/**
 * Redirect to admin/people/sync/case-1.
 */
function google_contacts_sync_drupal_to_google() {
  unset($_GET['destination']);
  drupal_goto('admin/people/sync/case-1');
}

/**
 * Redirect to admin/people/sync/case-2.
 */
function google_contacts_sync_google_to_drupal() {
  unset($_GET['destination']);
  drupal_goto('admin/people/sync/case-2');
}
