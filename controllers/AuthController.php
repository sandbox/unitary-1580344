<?php
class GoogleContactsSyncAuthController {
  /**
   * @path => GOOGLE_CONTACTS_SYNC_REDIRECT_URI
   * @access callback => TRUE
   */
  public function authAction() {
    if (!isset($_GET['code'])) {
      drupal_access_denied();
    }
    $client = GoogleContactsSyncGoogleClientFactory::createClient();
    $client->authenticate();
    $_SESSION[GOOGLE_CONTACTS_SYNC_SESSION_TOKEN] = $client->getAccessToken();
    if (isset($_GET['state'])) {
      $_GET['destination'] = $_GET['state'];
      drupal_goto('<front>');
    }
  }
}
