<?php
class GoogleContactsSyncAccountSyncController {
  /**
   * @path => 'sync-test',
   * @title => 'Sync test',
   * @type => MENU_NORMAL_ITEM,
   * @access arguments => array('administer site configuration'),
   */
  public function indexAction() {
    require_once drupal_get_path('module', 'google_contacts_sync') . '/forms/accounts_form.inc';
    return drupal_get_form('google_contacts_sync_accounts_form');
  }

  /**
   * @path => 'admin/people/sync/case-1',
   * @access arguments => array('administer site configuration'),
   */
  public function case1Action() {
    $this->doUpdate(new GoogleContactsSyncGContactUpdatingService());
  }

  /**
   * @path => 'admin/people/sync/case-2',
   * @access arguments => array('administer site configuration'),
   */
  public function case2Action() {
    $this->doUpdate(new GoogleContactsSyncDrupalUserUpdatingService());
  }

  protected function doUpdate($updating_service, $redirect_uri = NULL) {
    $ex = NULL;
    try {
      $client = $this->createClient($redirect_uri);
      $api = new GoogleContactsSyncContactsApi($client);
      $result = $updating_service->updateAll($api);
    }
    catch (GoogleContactsSyncEmptySettingException $e) {
      drupal_set_message(t("Please ensure that all Authentication/Authorization settings are not empty."), 'warning');
      $ex = TRUE;
    }
    catch (ApiAuthException $e) {
      drupal_set_message(t("Unable to authenticate. Please check the Authentication/Authorization settings."), 'warning');
      $ex = TRUE;
    }
    if ($ex) {
      $this->redirectToSettingsPage();
    }

    drupal_set_message(t('Created %count contact(s).', array('%count' => $result['created'])));
    drupal_set_message(t('Updated %count contact(s).', array('%count' => $result['updated'])));
    drupal_goto('sync-test');
  }

  protected function createClient($redirect_uri = NULL) {
    try {
      return GoogleContactsSyncGoogleClientFactory::createAuthorizedClient($redirect_uri);
    }
    catch (GoogleContactsSyncAuthorizationException $e) {
      drupal_set_message(t("Unable to authenticate. Please check the Authentication/Authorization settings."), 'warning');
      $this->redirectToSettingsPage();
    }
  }

  protected function redirectToSettingsPage() {
    drupal_goto('admin/config/people/gsync');
  }
}

