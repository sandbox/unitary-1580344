<?php
class GoogleContactsSyncSettingsController {
  /**
   * @path => 'admin/config/people/gsync',
   * @type => MENU_NORMAL_ITEM,
   * @title => 'Google Contacts Synchronization',
   * @access arguments => array('administer site configuration'),
   */
  public function indexAction() {
    require_once drupal_get_path('module', 'google_contacts_sync') . '/forms/settings_form.inc';
    return drupal_get_form('google_contacts_sync_settings_form');
  }
}
