<?php

/**
 * @file
 * Forms for pages.
 */

/**
 * Form for sync-test page.
 */
function google_contacts_sync_accounts_form() {
  $form = array();
  $form['dsync'] = array(
    '#type' => 'submit',
    '#value' => 'Drupal -> Google (Use Case #1)',
    '#submit' => array('google_contacts_sync_accounts_form_dsync_submit'),
  );
  $form['gsync'] = array(
    '#type' => 'submit',
    '#value' => 'Google -> Drupal (Use Case #2)',
    '#submit' => array('google_contacts_sync_accounts_form_gsync_submit'),
  );
  return $form;
}

/**
 * Submit for "Drupal -> Google (Use Case #1)" on sync-test page form.
 */
function google_contacts_sync_accounts_form_dsync_submit() {
  google_contacts_sync_authorize('admin/people/sync/case-1');
}

/**
 * Submit for "Google -> Drupal (Use Case #2)" on sync-test page form.
 */
function google_contacts_sync_accounts_form_gsync_submit() {
  google_contacts_sync_authorize('admin/people/sync/case-2');
}

/**
 * Redirect function.
 */
function google_contacts_sync_authorize($redirect_uri) {
  drupal_goto($redirect_uri);
  exit();
}
