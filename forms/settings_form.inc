<?php
/**
 * @file
 * Defines the 'google_contacts_sync_settings_form' and respective handlers.
 */

/**
 * Defines the 'google_contacts_sync_settings_form'.
 */
function google_contacts_sync_settings_form() {
  $form = array();

  google_contacts_sync_settings_form_add_authorization_group($form);

  $form = system_settings_form($form);
  $form['#submit'] = array('google_contacts_sync_settings_form_submit');

  return $form;
}

/**
 * Submit callback for the 'google_contacts_sync_settings_form'.
 */
function google_contacts_sync_settings_form_submit(array $form, array &$form_state) {
  // We need reset current token stored in $_SESSION after form submitting,
  // to apply settings.
  unset($_SESSION[GOOGLE_CONTACTS_SYNC_SESSION_TOKEN]);
  return system_settings_form_submit($form, $form_state);
}

/**
 * Adds the 'Authentication/Authorization settings' group to the form.
 */
function google_contacts_sync_settings_form_add_authorization_group(array &$form) {
  $group_name = 'authorization';
  $options = array('external' => TRUE);
  $link = l(t('Google APIs Console'), 'https://code.google.com/apis/console', $options);
  $form[$group_name] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentication/Authorization settings'),
    '#collapsible' => TRUE,
    '#description' => t("The settings should be the same as set on the $link page."),
  );
  $form[$group_name]['google_contacts_sync_app_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Application Name'),
    '#required' => TRUE,
    '#default_value' => variable_get('google_contacts_sync_app_name'),
  );
  $form[$group_name]['google_contacts_sync_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('google_contacts_sync_client_id'),
  );
  $form[$group_name]['google_contacts_sync_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#required' => TRUE,
    '#default_value' => variable_get('google_contacts_sync_client_secret'),
  );
  $form[$group_name]['google_contacts_sync_redirect_uri'] = array(
    '#type' => 'item',
    '#title' => 'Redirect URI',
    '#markup' => check_plain(GOOGLE_CONTACTS_SYNC_ABS_REDIRECT_URI),
  );
}
