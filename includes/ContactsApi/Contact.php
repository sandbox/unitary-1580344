<?php
/**
 * @see https://developers.google.com/gdata/docs/2.0/elements?hl=en#gdContactKind
 */
class GoogleContactsSyncContactsApiContact extends apiModel {
  /**
   * read-only
   */
  protected $title;

  protected $fullName = array();

  protected $email = array();

  protected $organization = array();

  protected $phoneNumber = array();

  protected $postalAddress = array();

  protected $nickname;

  public function __get($name) {
    if (!property_exists($this, $name)) {
      throw new RuntimeException("Invalid property name $name.");
    }
    return $this->$name;
  }

  public function __set($name, $value) {
    if (!property_exists($this, $name)) {
      throw new RuntimeException("Invalid property name $name.");
    }
    $this->$name = $value;
  }
}

