<?php
require_once drupal_get_path('module', 'google_contacts_sync') . '/lib/google-api-php-client/src/apiClient.php';
require_once drupal_get_path('module', 'google_contacts_sync') . '/lib/google-api-php-client/src/contrib/apiPlusService.php';
require_once drupal_get_path('module', 'google_contacts_sync') . '/lib/google-api-php-client/src/contrib/apiOauth2Service.php';

class GoogleContactsSyncGoogleClientFactory {
  public static function createClient() {
    $client = new apiClient();
    $client->setApplicationName(self::getSetting('app_name'));
    $client->setClientId(self::getSetting('client_id'));
    $client->setRedirectUri(GOOGLE_CONTACTS_SYNC_ABS_REDIRECT_URI);
    $client->setClientSecret(self::getSetting('client_secret'));
    $client->setScopes(array(
      'https://www.google.com/m8/feeds/',
    ));
    return $client;
  }

  public static function createAuthorizedClient($redirect_after_to = NULL) {
    $client = self::createClient();
    self::authorize($client, $redirect_after_to);
    if (!isset($_SESSION[GOOGLE_CONTACTS_SYNC_SESSION_TOKEN])) {
      throw new GoogleContactsSyncAuthorizationException();
    }
    $client->setAccessToken($_SESSION[GOOGLE_CONTACTS_SYNC_SESSION_TOKEN]);
    return $client;
  }

  protected static function getSetting($name) {
    $var_name = 'google_contacts_sync_' . $name;
    $var_value = variable_get($var_name);
    if (empty($var_value)) {
      throw new GoogleContactsSyncEmptySettingException();
    }
    return $var_value;
  }

  protected static function authorize(apiClient $client, $redirect_after_to) {
    // @TODO: move to authorization/authentication service?
    if (!isset($_SESSION[GOOGLE_CONTACTS_SYNC_SESSION_TOKEN])) {
      if (empty($redirect_after_to)) {
        $redirect_after_to = $_GET['q'];
      }
      $client->setState($redirect_after_to);
      $auth_uri = $client->createAuthUrl();
      $response = drupal_http_request($auth_uri);
      if ($response->code != 200) {
        throw new GoogleContactsSyncAuthorizationException();
      }
      drupal_goto($auth_uri);
    }
  }
}
