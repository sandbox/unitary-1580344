<?php
// @TODO: extends apiServiceResource?
class GoogleContactsSyncContactsApi {
  private $client;

  public function __construct(apiClient $client) {
    if (!$client->getAccessToken()) {
      throw new RuntimeException("The \$client must have access token.");
    }
    $this->client = $client;
  }

  /**
   * @return bool
   */
  public function createContact(GoogleContactsSyncContactsApiContact $contact, $user_email = NULL) {
    $doc = new DOMDocument();
    $doc->formatOutput = TRUE;
    $doc->encoding = 'utf-8';

    $entry = $doc->createElement('atom:entry');
    $doc->appendChild($entry);
    $entry->setAttributeNS(
      'http://www.w3.org/2000/xmlns/',
      'xmlns:atom',
      'http://www.w3.org/2005/Atom'
    );
    $entry->setAttributeNS(
      'http://www.w3.org/2000/xmlns/' ,
      'xmlns:gd',
      'http://schemas.google.com/g/2005'
    );
    $entry->setAttributeNS(
      'http://www.w3.org/2000/xmlns/',
      'xmlns:gContact',
      'http://schemas.google.com/contact/2008'
    );

    // @TODO: do we need to add the following element?
    //   <atom:category scheme='http://schemas.google.com/g/2005#kind'
    //    term='http://schemas.google.com/contact/2008#contact'/>

    $nickname = $doc->createElement('gContact:nickname', $contact->nickname);
    $entry->appendChild($nickname);

    if (count($contact->fullName)) {
        // /entry/gd:name
      $name = $doc->createElement('gd:name');
      $entry->appendChild($name);

      // /entry/gd:name/gd:fullName
      $value = $contact->fullName[0];
      $fullName = $doc->createElement('gd:fullName', $value);
      $name->appendChild($fullName);
    }

    if (count($contact->email)) {
      // /entry/gd:email
      $email = $doc->createElement('gd:email');
      $entry->appendChild($email);
      $value = $contact->email[0];
      $email->setAttribute('address', $value);
      $email->setAttribute('rel', 'http://schemas.google.com/g/2005#work');
    }

    if (count($contact->phoneNumber)) {
      // /entry/gd:phoneNumber
      $value = $contact->phoneNumber[0];
      $phoneNumber = $doc->createElement('gd:phoneNumber', $value);
      $entry->appendChild($phoneNumber);
      $phoneNumber->setAttribute('primary', 'true');
      $phoneNumber->setAttribute('rel', 'http://schemas.google.com/g/2005#work');
    }

    if (count($contact->postalAddress)) {
      // /entry/gd:structuredPostalAddress
      $structuredPostalAddress = $doc->createElement('gd:structuredPostalAddress');
      $entry->appendChild($structuredPostalAddress);
      $structuredPostalAddress->setAttribute('rel', 'http://schemas.google.com/g/2005#work');
      $structuredPostalAddress->setAttribute('primary', 'true');

      // entry/gd:structuredPostalAddress/gd:formattedAddress
      $value = $contact->postalAddress[0];
      $formattedAddress = $doc->createElement('gd:formattedAddress', $value);
      $structuredPostalAddress->appendChild($formattedAddress);
    }

    if (count($contact->organization)) {
      // /entry/gd:organization
      $org = $doc->createElement('gd:organization');
      $org->setAttribute('rel' ,'http://schemas.google.com/g/2005#work');
      $entry->appendChild($org);

      // entry/gd:organization/gd:orgName
      $value = $contact->organization[0];
      $orgName = $doc->createElement('gd:orgName', $value);
      $org->appendChild($orgName);
    }

    $post_body = $doc->saveXML();
    //d()->varDump($post_body);

    if (!$user_email) {
      $user_email = 'default';
    }
    $uri = "https://www.google.com/m8/feeds/contacts/$user_email/full";
    $headers = array(
      'GData-Version' => '3.0',
      'Content-Type' => 'application/atom+xml'
    );
    $response = $this->request($uri, 'post', $headers, $post_body);
    if (empty($response['id'])) {
      throw new RuntimeException("Unable to create Contact.");
    }
    return TRUE;
  }

  /**
   * @return GoogleContactsSyncContactsApiContact
  public function getContact($contactId, $user_email = NULL) {
    if (!$user_email) {
      $user_email = 'default';
    }
    $uri = "https://www.google.com/m8/feeds/contacts/$user_email/full/" . $contactId . '?alt=json';
    $response = $this->request($uri, 'get');
    // @TODO: Handle case if Contact with ID does not exist.
    if (!isset($response['entry'])) {
      throw new RuntimeException("Mailformed response.");
    }
    return $this->createContactFromFeedEntry($response['entry']);
  }
  */

  /**
   * @return array An array of GoogleContactsSyncContactsApiContact objects.
   */
  public function listAllContacts($user_email = NULL) {
    // Note: The feed may not contain all of the user's contact groups, because
    // there's a default limit on the number of results returned. For more
    // information, see the max-results query parameter.
    if (!$user_email) {
      $user_email = 'default';
    }
    $uri = "https://www.google.com/m8/feeds/contacts/$user_email/full?alt=json";
    $response = $this->request($uri);
    $contacts = array();
    if (!$response) {
      return $contacts;
    }
    if (!isset($response['feed'])) {
      throw new RuntimeException("Mailformed response.");
    }
    $entries = isset($response['feed']['entry'])
      ? $response['feed']['entry']
      : array();
    foreach ($entries as $entry) {
      $contact = $this->createContactFromFeedEntry($entry);
      $contacts[]= $contact;
    }
    return $contacts;
  }

  protected function createContactFromFeedEntry(array $entry) {
    $contact = new GoogleContactsSyncContactsApiContact();
    $contact->title = $entry['title']['$t'];
    $this->addField($contact, 'email', $entry, 'address')
      ->addField($contact, 'phoneNumber', $entry)
      ->addField($contact, 'organization', $entry)
      ->addField($contact, 'postalAddress', $entry);
    // @TODO: nickname?
    return $contact;
  }

  private function addField(GoogleContactsSyncContactsApiContact $contact, $field_name, array $entry, $item_key = '$t') {
    $field_value = array();
    $key = 'gd$' . $field_name;
    if (!isset($entry[$key])) {
      $entry[$key] = array();
    }
    foreach ($entry[$key] as $item) {
      if (isset($item[$item_key])) {
        $field_value[] = $item[$item_key];
      }
    }
    $contact->{$field_name} = $field_value;
    return $this;
  }

  /**
   * @return array|NULL
   */
  private function request($uri, $method = 'GET', array $headers = array(), $post_body = NULL) {
    $req = new apiHttpRequest($uri, strtoupper($method), $headers, $post_body);
    $this->client->getIo()->authenticatedRequest($req);
    $response_code = $req->getResponseHttpCode();
    //d($req->getResponseBody());
    if ($response_code != 200 && $response_code != 201) {
      throw new RuntimeException("Invalid request, response code: " . $response_code);
    }
    $response = trim($req->getResponseBody());
    $is_xml = substr($response, 0, 5) == '<?xml';
    if ($is_xml) {
      $response = json_encode(simplexml_load_string($response));
    }
    return json_decode($response, TRUE);
  }
}

