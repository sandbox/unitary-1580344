<?php
abstract class GoogleContactsSyncPeopleUpdatingService {
  const PEOPLE_UPDATED = 'updated';
  const PEOPLE_CREATED = 'created';

  abstract public function updateAll(GoogleContactsSyncContactsApi $api);
  
  protected function initResult() {
    return array(
      self::PEOPLE_UPDATED => 0,
      self::PEOPLE_CREATED => 0,
    );
  }
}
