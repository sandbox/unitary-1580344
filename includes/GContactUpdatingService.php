<?php
class GoogleContactsSyncGContactUpdatingService extends GoogleContactsSyncPeopleUpdatingService {
  public function updateAll(GoogleContactsSyncContactsApi $api) {
    $contacts = $api->listAllContacts();
    $users_lines = db_select('users', 'users')
      ->fields('users', array('uid', 'mail'))
      ->condition('uid', 0, '!=')
      ->execute()
      ->fetchAllKeyed();
    $result = $this->initResult();
    $op = self::PEOPLE_CREATED;
    foreach ($users_lines as $uid => $user_email) {
      $found = FALSE;
      foreach ($contacts as $contact) {
        if (in_array($user_email, $contact->email)) {
          $found = TRUE;
          break;
        }
      }
      if (!$found) {
        $was_created = $this->createContactForUser($api, $uid);
        if (!$was_created) {
          continue;
        }
        if (!isset($result[$op])) {
          $result[$op] = 0;
        }
        $result[$op]++;
      }
    }
    return $result;
  }

  /**
   * @return bool
   */
  protected function createContactForUser(GoogleContactsSyncContactsApi $api, $uid) {
    $user = user_load($uid);
    $contact = $this->userToContact($user);
    $result = $api->createContact($contact);
    return $result;
  }
  
  protected function userToContact(stdClass $user) {
    $contact = new GoogleContactsSyncContactsApiContact(array(
      'fullName' => $this->getCompoundFieldValue($user, 'full_name'),
      'organization' => $this->getCompoundFieldValue($user, 'organization'),
      'phoneNumber' => $this->getCompoundFieldValue($user, 'phone_number'),
      'postalAddress' => $this->getCompoundFieldValue($user, 'postal_address'),
      'email' => array(
        $user->mail
      ),
      'nickname' => $user->name,
    ));
    return $contact;
  }

  /**
   * @return array
   */
  protected function getCompoundFieldValue(stdClass $user, $field_name, array $default_value = array()) {
    $lang_code = !empty($user->language) ? $user->language : LANGUAGE_NONE;
    $field_name = 'field_' . $field_name;
    if (!isset($user->$field_name)) {
      throw new InvalidArgumentException("The invalid field name '$field_name' was provided.");
    }
    if (isset($user->{$field_name}[$lang_code])) {
      $field_value = $user->{$field_name}[$lang_code];
    }
    else {
      $field_value = $default_value;
    }
    $value = array();
    foreach ($field_value as $item) {
      $value[] = $item['value'];
    }
    return $value;
  }

  private function toScalar(array $value, $default_value = '') {
    return count($value) ? array_shift($value) : $default_value;
  }
}
