<?php
class GoogleContactsSyncDrupalUserUpdatingService extends GoogleContactsSyncPeopleUpdatingService {
  public function updateAll(GoogleContactsSyncContactsApi $api) {
    $contacts = $api->listAllContacts();
    $result = $this->initResult();
    foreach ($contacts as $contact) {
      $op = $this->updateAccount($contact);
      $result[$op]++;
    }
    return $result;
  }

  /**
   * @return bool Returns TRUE if Drupal user was updated.
   */
  protected function updateAccount(GoogleContactsSyncContactsApiContact $contact) {
    $uid = db_select('users', 'users')
      ->fields('users', array('uid'))
      ->condition('mail', $contact->email, 'IN')
      ->execute()
      ->fetchField();
    if ($uid) {
      $this->updateUser($contact, $uid);
      return self::PEOPLE_UPDATED;
    }
    else {
      $this->createUser($contact);
      return self::PEOPLE_CREATED;
    }
  }

  protected function createUser(GoogleContactsSyncContactsApiContact $contact) {
    $user = new stdClass();
    if (empty($contact->email[0])) {
      throw new RuntimeException("Contact should have at least one email address.");
    }
    if (!empty($contact->nickname)) {
      $user->name = $contact->nickname;
    }
    else {
      $user->name = $contact->email[0];
    }
    $user->mail = $contact->email[0];
    $user->pass = '123';  // @TODO: What we should use here?
    $user->notify = 0;
    $user->status = 1; // 0 - block, 1 - active.
    $result = user_save($user);
    if (!$result) {
      throw new RuntimeException("Unable to save user.");
    }
  }

  protected function updateUser(GoogleContactsSyncContactsApiContact $contact, $uid) {
    $user = user_load($uid);
    // @TODO: Do we need overwrite existing fields and add new fiels
    // or do we need only add new fields?
    $account_data = array(
    );
    $result = user_save($user, $account_data);
    if (!$result) {
      throw new RuntimeException("Unable to save user.");
    }
  }
}

